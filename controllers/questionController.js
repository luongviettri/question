export const checkAnswerRadioButton = () => {

    // ! i am no hiểu

    if (!document.querySelector('input[name="singleChoice"]:checked')) {
        return false;
    }
    let userAnswer = document.querySelector('input[name="singleChoice"]:checked').value;
    return userAnswer == "true";
}
const renderRadioButton = (data) => {
    return `
    <div class="btn-group col-12 text-left" data-toggle="buttons">
        <label class="btn  text-left">
            <input type="radio" value=${data.exact} name="singleChoice" id="" autocomplete="off"> ${data.content}
        </label>
    </div>
    `
}
export const renderSingleChoice = (question) => {
    // ! Hiển thị câu trả lời
    let contentAnswer = "";
    question.answers.forEach((item) => {
        contentAnswer += renderRadioButton(item);
    })
    return `
    <div>
    <p>${question.content} </p>
    <div class="row" >${contentAnswer} </div>
    </div>
    `
}
export const renderFillToInput = (question) => {
    return `
    <div>
        <p>${question.content} </p>
        <div class="form-group">
            <input type="text"
            data-not-answer='${question.answers[0].content}'
            class="form-control" name="" id="fillToInput"  placeholder="Câu trả lời"/>
        </div> 
    </div>
    `
}
export const renderQuestion = (question) => {
    if (question.questionType == 1) {
        document.getElementById("contentQuiz").innerHTML = renderSingleChoice(question);
    } else {
        document.getElementById("contentQuiz").innerHTML = renderFillToInput(question);
    }
}
export const checkFillToInput = () => {
    let inputEl = document.getElementById('fillToInput');
    return inputEl.dataset.notAnswer == inputEl.value;
}
export const showResult = () => {
    document.getElementById('endQuiz').style.display = "block";
    document.getElementById('startQuiz').style.display = "none";
    
}