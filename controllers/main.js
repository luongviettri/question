import { data } from "../data/questions.js"
import { renderQuestion, checkAnswerRadioButton, checkFillToInput, showResult } from "./questionController.js"

let currentQuestionIndex = 0;
let arrQuestion = data.map((question) => {
    return { ...question, isCorrect: false };
})

console.log('arrQuestion:>>>>>>>>>> ', arrQuestion);

// ! render lần đầu
renderQuestion(arrQuestion[currentQuestionIndex]);
// ! show số thứ tự
document.getElementById('currentStep').innerHTML =
    `
<h2>
${currentQuestionIndex + 1}/${arrQuestion.length}
</h2>
`;
document.getElementById("nextQuestion").addEventListener('click', () => {
    // ! khi làm xong câu cuối cùng
    if (arrQuestion[currentQuestionIndex].questionType == 1) {
        arrQuestion[currentQuestionIndex].isCorrect = checkAnswerRadioButton();
    } else {
        arrQuestion[currentQuestionIndex].isCorrect = checkFillToInput();
    }
    if (currentQuestionIndex >= arrQuestion.length - 1) {
        showResult();
        return;
    }
    currentQuestionIndex++;
    // ! show số thứ tự
    document.getElementById('currentStep').innerHTML =
        `
    <h2>
    ${currentQuestionIndex + 1}/${arrQuestion.length}
    </h2>
    `;
    renderQuestion(arrQuestion[currentQuestionIndex]);
})
